export interface IHeroes {
    name?: string;
    aliases?: [String];
    occupation?: string;
    gender?: string;
    height?: {
        ft: number,
        in: number
    };
    hair?: string;
    eyes?: string;
    powers?: [String];
}

