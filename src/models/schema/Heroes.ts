import { Document, Schema, Model, model } from 'mongoose';
import { IHeroes } from '../interface/Heroes';

export interface IHeroesModel extends IHeroes, Document {
    nameFull() : string;
}

export const HeroesSchema: Schema = new Schema({
    createdAt: Date,
    name: String,
    aliases: [String],
    occupation: String,
    gender: String,
    height: {
        ft: Number,
        in: Number
    },
    hair: String,
    eyes: String,
    powers: [String]
});

HeroesSchema.pre<IHeroesModel>('save', (next) : void => {
   let now : Date = new Date();
   if (!this.createdAt) {
       this.createdAt = now;
   }
   next();
})

HeroesSchema.methods.nameFull = () : String => {
   return (this.name.trim() + ' ' + this.aliases.map((alias : string):string => alias.trim()));
}

export const Heroes : Model<IHeroesModel> = model<IHeroesModel>('Heroes', HeroesSchema);

