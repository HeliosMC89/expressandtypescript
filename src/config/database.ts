import * as mongoose from 'mongoose';
import  bluebird = require('bluebird');
import Environment from './environment';
export const init = () => {
        (<any>mongoose).Promise = bluebird;
        mongoose.connect(Environment.databaseUri, {
            useNewUrlParser : true
        });

        mongoose.connection.on("error", () : void => {
            console.debug("Could not connecting the database. Existing Now...");
            process.exit();
        });

        mongoose.connection.once("open", () : void => {
            console.debug("Successfully connected to the database");
        });
}