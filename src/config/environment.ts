export default class Environment {
    public static port: number = 8080;
    public static bodyLimit: string = "100kb";
    public static databaseUri: string = "mongodb://127.0.0.1:27017/superHeroes";
	public static corsHeaders: Array<string> = ["Link"];
};

