import { Router, Request, Response, NextFunction } from 'express';
import { Heroes, IHeroesModel } from '../models/schema/Heroes';

export class HeroRouter {
    router: Router;
    /**
     * Initialize the HeroRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    /**
     * GET all Heroes.
     */
    private async getAll(request: Request, response: Response, next: NextFunction) : Promise<any> {
        try {
            const heroes : IHeroesModel[] = await Heroes.find();
            if (heroes.length) {
                return response.status(200).jsonp({
                    status: 'success',
                    data: heroes
                });
            } else {
                return response.status(200).jsonp({
                    status: 'success',
                    message: 'Heroes empty',
                    data: heroes
                })
            }
        } catch (err) {
            return response.status(400).jsonp({
                status: 'fail',
                message: err.message || 'Error in the server'
            })
        }
    }

    /**
     *  Create Heroes 
     */

    private async store(request: Request, response: Response, next: NextFunction) : Promise<any>  {
        try {
            const heroe : IHeroesModel = await Heroes.create(request.body);
            if (heroe) {
                response.status(201).jsonp({
                    status: 'success',
                    data: heroe
                });
            } else {
                response.status(400).jsonp({
                    status: 'fail',
                    data: heroe
                });
            }
        } catch (err) {
            response.status(400).jsonp({
                status: 'fail',
                message:  err.message || 'Error en el request.body' 
            });
        }
    }
    /**
     * Get Heroe by ID
     */
    private async getOne(request: Request, response: Response, next: NextFunction) : Promise<any>  {
        try {
            const id : number = request.params.id;
            const heroe = await Heroes.findById(id);    
            if (heroe) {
                response.status(200).jsonp({
                    status: 'success',
                    data: heroe
                })
            } else {
               response.status(404).jsonp({
                   status: 'fail',
                   message: 'Heroe not found'
               })
            }
        } catch (error) {
            response.status(400).jsonp({
                status: 'fail',
                message: error.message || 'Error param ID'
            })
        }
        
    }

    private async deleteOne(request: Request, response: Response, next: NextFunction) : Promise<any> {
        try {
            const id : number = request.params.id;
            const heroe : IHeroesModel = await Heroes.findByIdAndDelete(id);
            if (heroe) {
                response.status(200).jsonp({
                    status: 'success',
                    message: 'Heroe Delete',
                    data: heroe
                })
            } else {
               response.status(404).jsonp({
                   status: 'fail',
                   message: 'Not exist',
                   data: heroe
               }) 
            }
        } catch (error) {
            response.status(400).jsonp({
                status: 'fail',
                message: error.message || 'Error param ID'
            });
        }
    }

    private async updateOne(request: Request, response: Response, next: NextFunction) : Promise<any> {
       try {
        const id : number = request.params.id;
        const heroe : IHeroesModel = await Heroes.findByIdAndUpdate(id, request.body);
        if (heroe) {
            response.status(200).jsonp({
                status: 'success',
                message: 'Updated Heroe successfully',
                data: heroe
            })
        } else {
            response.status(404).jsonp({
                status: 'fail',
                message: 'Heroe not found',
                data: heroe
            })
        }
       } catch (err) {
          response.status(400).jsonp({
              status: 'fail',
              message: err.message || 'Error param ID'
          }) 
       }
    }

    public init() : void {
        this.router.get('/', this.getAll);
        this.router.get('/:id', this.getOne);
        this.router.put('/:id', this.updateOne);
        this.router.delete('/:id', this.deleteOne);
        this.router.post('/', this.store);
    }
}

// Create the HeroRouter, and exports its configured Express.Router
const heroRoutes = new HeroRouter();
heroRoutes.init();

export default heroRoutes.router;