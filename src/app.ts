import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as db from './config/database';
import HeroRouter from './routes/HeroRouter';

// Creates and configures an ExpressJS web server.
class App {
    
    // ref to Express instance
    public express: express.Application;

    // Run configuration methods on the Express instance.
    constructor () {
        this.express = express();
        this.database();
        this.middleware();
        this.routes();
    }
    
    // Configure MongoDB 
    private database() : any {
        return db.init();
    }

    // Configure Express middleware
    private middleware(): void {
       
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(cors());
    }

    // Configure API endpoints.
    private routes(): void {
        /* This is just to get up and running, and to make sure what we've got is
         * working so far. This function will change when we start to add more
         * API endpoints */
        let router = express.Router();
        // placeholder route handler
        router.get('/', async (request, response, next) => {
            response.status(200).jsonp({
                message: 'Hello World!'
            });
        });
        this.express.use('/', router);
        this.express.use('/api/v1/heroes', HeroRouter);
    }

}


export default new App().express;